#!/usr/bin/python3
from sys import argv
from sys import stderr

if len(argv)>1:
    try:
        r=1.0/255.0*int(argv[1][0:2],16)
        g=1.0/255.0*int(argv[1][2:4],16)
        b=1.0/255.0*int(argv[1][4:6],16)
        print(str(r)[0:6]+","+str(g)[0:6]+","+str(b)[0:6])
    except:
        stderr.write("InvalidColorError\n")
        exit(1)
else:
    stderr.write("ColorNotFoundError\n")
    exit(1)
exit(0)
