#include <iostream>
#include <cmath>

using namespace std;

namespace bin{
	
	bool getBit(unsigned int variable,unsigned char index){
		unsigned int tmp=1<<index;
		return (tmp&variable)>0;
	}

	void setBit(unsigned int& variable,unsigned char index,bool value){
		unsigned int tmp=1<<index;
		if( (tmp&variable) > 0 && !value){
			variable-=tmp;
		}if( (tmp&variable) ==0 && value ){
			variable+=tmp;
		}
	}
	
	unsigned int getInt(unsigned int variable,unsigned char from,unsigned char bits){
		unsigned int tmp=pow(2,bits)-1;
		tmp=tmp<<from;
		tmp=tmp&variable;
		tmp=tmp>>from;
		return tmp;
	}

	bool setInt(unsigned int& variable,unsigned char from,unsigned char bits,unsigned int value){
		unsigned int tmp=pow(2,bits)-1;
		if(tmp >= value){
			tmp=tmp<<from;
			variable&=~tmp;
			value=value<<from;
			variable+=value;
			return true;
		}else{return false;}
	}

	void print(unsigned int variable){
		for(unsigned char i=32;i>0;i--){
			cout << getBit(variable,i-1);
		}
		cout << "\n";
	}
}
