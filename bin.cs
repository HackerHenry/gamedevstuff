using System; 

namespace bin{public class Bin{	
	static public bool GetBit(uint variable,byte index){
		uint tmp=(uint)(1<<index);
		return (tmp&variable)>0;
	}

	static public void SetBit(ref uint variable,byte index,bool value){
		uint tmp=(uint)(1<<index);
		if( (tmp&variable) > 0 && !value){
			variable-=tmp;
		}if( (tmp&variable) ==0 && value ){
			variable+=tmp;
		}
	}
	
	static public uint GetInt(uint variable,byte from,byte bits){
		uint tmp=(uint)(Math.Pow(2,bits)-1);
		tmp=tmp<<from;
		tmp=tmp&variable;
		tmp=tmp>>from;
		return tmp;
	}

	static public bool SetInt(ref uint variable,byte from,byte bits,uint value){
		uint tmp=(uint)(Math.Pow(2,bits)-1);
		if(tmp >= value){
			tmp=tmp<<from;
			variable&=~tmp;
			value=value<<from;
			variable+=value;
			return true;
		}else{return false;}
	}
}}
